﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorBehaviour : MonoBehaviour
{
    public string SceneName;
    public bool IsClickable;
    public bool lockDoor;
    private void OnTriggerEnter(Collider other)
    {
        if (!lockDoor)
        {
            OpenOnProximity();
        }
        else
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (lockDoor)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);

        }
        else
        {
            return;
        }
    }

    void OpenOnProximity()
    {
        SceneManager.LoadScene(SceneName);
    }

    private void OnMouseDown()
    {
        if (IsClickable)
        {
            SceneManager.LoadScene(SceneName);
        }
        else
        {
            return;
        }
    }
}
